import { Express, Request, Response, NextFunction } from 'express';
interface Options {
    folderPath?: string;
    apiBasePath?: string;
    callBack?: (app: Express) => void;
    middlewareMapping?: {
        [key: string]: Middleware;
    };
    log?: boolean;
}
declare type Middleware = (req: Request, res: Response, next: NextFunction) => void;
declare type HandlerFunction<QueryProps, BodyProps, OutputProps> = (req: Omit<Request, 'query' | 'body'> & {
    query: QueryProps;
    body: BodyProps;
}, res: Omit<Response, 'json'> & {
    json: (output: OutputProps) => void;
}, next?: NextFunction) => void;
export interface Route<MiddlewareNames, QueryProps, BodyProps, OutputProps> {
    prefix?: string;
    suffix?: string;
    middlewares?: (MiddlewareNames | Middleware)[];
    handler: HandlerFunction<QueryProps, BodyProps, OutputProps>;
}
export default function autoRouting(_app: Express, _options?: Options): Promise<void>;
export {};
