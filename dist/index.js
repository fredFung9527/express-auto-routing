"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const fs_1 = require("fs");
const process_1 = require("process");
let app;
let options;
const methods = ['all', 'get', 'post', 'patch', 'delete'];
const acceptFiles = ['.js', '.ts'];
function getPath(parent, name) {
    const finalName = name[0] === '_' ? `:${name.substring(1)}` : name;
    const parentPart = `${parent}${parent === '/' ? '' : '/'}`;
    let result = `${parentPart}${finalName}`;
    if (result.endsWith('/index')) {
        result = result.substring(0, result.length - 6);
    }
    return result || '/';
}
function tryCatchWrapper(f) {
    return function () {
        return __awaiter(this, arguments, void 0, function* () {
            try {
                yield f.apply(this, arguments);
            }
            catch (e) {
                arguments[2](e);
            }
        });
    };
}
function getMiddleware(v) {
    if (typeof v === 'string') {
        const f = (options === null || options === void 0 ? void 0 : options.middlewareMapping) && (options === null || options === void 0 ? void 0 : options.middlewareMapping[v]);
        if (!f) {
            throw new Error(`Cannot find the middleware with name: ${v}`);
        }
        return f;
    }
    else {
        return v;
    }
}
function getMiddlewares(v) {
    if (!(v === null || v === void 0 ? void 0 : v.length)) {
        return [];
    }
    return v.map(f => getMiddleware(f));
}
function register(filePath, apiPath) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        if (!acceptFiles.includes((0, path_1.extname)(filePath))) {
            return;
        }
        const method = (0, path_1.parse)(filePath).name.toLocaleLowerCase();
        if (!methods.includes(method)) {
            return;
        }
        const theRoute = (_a = (yield Promise.resolve().then(() => require(filePath)))) === null || _a === void 0 ? void 0 : _a.route;
        if (!theRoute) {
            return;
        }
        const path = `${theRoute.prefix || ''}${apiPath}${theRoute.suffix || ''}`;
        const middlewares = getMiddlewares(theRoute.middlewares);
        app[method](path, [...middlewares, theRoute.handler].map((f) => tryCatchWrapper(f)));
        if (options === null || options === void 0 ? void 0 : options.log) {
            console.log(`\x1b[32m${method} ${path}\x1b[0m is registered`);
        }
    });
}
function scanFolder(folderPath, apiPath = '/') {
    return __awaiter(this, void 0, void 0, function* () {
        const items = yield (0, fs_1.readdirSync)(folderPath, { withFileTypes: true });
        let dynamicPaths = [];
        for (const item of items) {
            const name = item.name;
            if (name[0] === '_') {
                dynamicPaths.push(item);
                continue;
            }
            const path = (0, path_1.resolve)(folderPath, name);
            if (item.isDirectory()) {
                yield scanFolder(path, getPath(apiPath, name));
            }
            else {
                yield register(path, apiPath);
            }
        }
        for (const item of dynamicPaths) {
            const name = item.name;
            const path = (0, path_1.resolve)(folderPath, name);
            if (item.isDirectory()) {
                yield scanFolder(path, getPath(apiPath, name));
            }
            else {
                yield register(path, apiPath);
            }
        }
    });
}
function autoRouting(_app, _options) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!_app) {
            return;
        }
        app = _app;
        options = _options;
        let finalFolderPath = (options === null || options === void 0 ? void 0 : options.folderPath) || './routes';
        if (!(0, path_1.isAbsolute)(finalFolderPath)) {
            if ((0, path_1.isAbsolute)(process.argv[1])) {
                finalFolderPath = (0, path_1.resolve)(process.argv[1], '..', finalFolderPath);
            }
            else {
                finalFolderPath = (0, path_1.resolve)((0, process_1.cwd)(), process.argv[1], '..', finalFolderPath);
            }
        }
        yield scanFolder(finalFolderPath, (options === null || options === void 0 ? void 0 : options.apiBasePath) || '/');
        (options === null || options === void 0 ? void 0 : options.callBack) && options.callBack(app);
    });
}
exports.default = autoRouting;
