### Features
- Register routes automatically
- Can set up base path
- Middleware shortcuts

### Download
`yarn add earf`

### Usage
#### In `./app.ts`
```
import express from 'express'
import autoRouting from 'earf'
import { notFoundHandler, errorHandler } from './middlewares/error'
import { testMiddleware } from './middlewares/testMiddleware'

const app = express()
autoRouting(app, {
  folderPath: resolve(__dirname, './routes'), // optional; default value is './routes'
  basePath: '/', // optional; default value is '/'
  callBack: (app) => { // optional
    app.use(notFoundHandler)
    app.use(errorHandler)
  }, 
  middlewareMapping: { // optional; common middlewares for future use
    testMiddleware
  }
})
```
#### For each `.ts` file in `./routes/`, whose name is in ['all' | 'get' | 'post' | 'patch' | 'delete']
```
import { MyRoute } from './../index'

export const route: MyRoute<any, any, any> = {
  handler: (req, res) => {
    console.log('aaaaaaaaaa')
    res.json('bbbbbbbbbb')
  }
}
```

### Others
Dynamic routing: name the file with prefix `_`
