import { Express, Request, Response, NextFunction } from 'express'
import { resolve, extname, parse, isAbsolute } from 'path'
import { readdirSync } from 'fs'
import { cwd } from 'process'

interface Options {
  folderPath?: string,
  apiBasePath?: string,
  callBack?: (app: Express) => void,
  middlewareMapping?: {
    [key: string]: Middleware
  },
  log?: boolean
}

type Middleware = (req: Request, res: Response, next: NextFunction) => void

type HandlerFunction<QueryProps, BodyProps, OutputProps> = (
  req: Omit<Request, 'query' | 'body'> & { query: QueryProps, body: BodyProps }, 
  res: Omit<Response, 'json'> & { json: (output: OutputProps) => void }, 
  next?: NextFunction
) => void

type Method = 'all' | 'get' | 'post' | 'patch' | 'delete'

export interface Route <MiddlewareNames, QueryProps, BodyProps, OutputProps> {
  prefix?: string,
  suffix?: string,
  middlewares?: (MiddlewareNames | Middleware)[],
  handler: HandlerFunction<QueryProps, BodyProps, OutputProps>
}

let app: Express
let options: Options
const methods: Method[] = ['all', 'get', 'post', 'patch', 'delete']
const acceptFiles = ['.js', '.ts'] 

function getPath(parent: string, name: string) {
  const finalName = name[0] === '_' ? `:${name.substring(1)}` : name
  const parentPart = `${parent}${parent === '/' ? '' : '/'}`
  let result = `${parentPart}${finalName}`
  if (result.endsWith('/index')) {
    result = result.substring(0, result.length - 6)
  }
  return result || '/'
}

function tryCatchWrapper(f: Middleware | HandlerFunction<any, any, any>) {
  return async function() {
    try {
      await f.apply(this, arguments)
    } catch (e) {
      arguments[2](e)
    }
  }
}

function getMiddleware(v: string | Middleware): Middleware {
  if (typeof v === 'string') {
    const f = options?.middlewareMapping && options?.middlewareMapping[v]
    if (!f) {
      throw new Error(`Cannot find the middleware with name: ${v}`)
    }
    return f
  } else {
    return v
  }
}

function getMiddlewares(v: (string | Middleware)[]): Middleware[] {
  if (!v?.length) {
    return []
  }
  return v.map(f => getMiddleware(f))
}

async function register(filePath: string, apiPath: string) {
  if (!acceptFiles.includes(extname(filePath))) {
    return
  }
  const method = parse(filePath).name.toLocaleLowerCase() as Method
  if (!methods.includes(method)) {
    return
  }
  const theRoute = (await import(filePath))?.route as Route<string, any, any, any>
  if (!theRoute) {
    return
  }
  
  const path = `${theRoute.prefix || ''}${apiPath}${theRoute.suffix || ''}`
  const middlewares = getMiddlewares(theRoute.middlewares)
  app[method](path, [...middlewares, theRoute.handler].map((f) => tryCatchWrapper(f)))

  if (options?.log) {
    console.log(`\x1b[32m${method} ${path}\x1b[0m is registered`)
  }
}

async function scanFolder(folderPath: string, apiPath='/') {
  const items = await readdirSync(folderPath, { withFileTypes: true })
  let dynamicPaths = []
  for (const item of items) {
    const name = item.name
    if (name[0] === '_') {
      dynamicPaths.push(item)
      continue
    }
    const path = resolve(folderPath, name)
    if (item.isDirectory()) {
      await scanFolder(path, getPath(apiPath, name))
    } else {
      await register(path, apiPath)
    }
  }
  for (const item of dynamicPaths) {
    const name = item.name
    const path = resolve(folderPath, name)
    if (item.isDirectory()) {
      await scanFolder(path, getPath(apiPath, name))
    } else {
      await register(path, apiPath)
    }
  }
}

export default async function autoRouting(_app: Express, _options?: Options) {
  if (!_app) {
    return
  }
  app = _app
  options = _options

  let finalFolderPath = options?.folderPath || './routes'
  if (!isAbsolute(finalFolderPath)) {
    if (isAbsolute(process.argv[1])) {
      finalFolderPath = resolve(process.argv[1], '..', finalFolderPath)
    } else {
      finalFolderPath = resolve(cwd(), process.argv[1], '..', finalFolderPath)
    }
  }
  await scanFolder(finalFolderPath, options?.apiBasePath || '/')
  options?.callBack && options.callBack(app)
}