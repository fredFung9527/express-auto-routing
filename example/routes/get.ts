import { MyRoute } from './../index'

interface QueryProps {
  test: string
}

interface BodyProps {
  test2: string
}

interface Output {
  test3: string
}

export const route: MyRoute<QueryProps, BodyProps, Output> = {
  handler: (req, res) => {
    console.log(req.query.test)
    console.log(req.body.test2)
    res.json({test3: 'aaa'})
  }
}