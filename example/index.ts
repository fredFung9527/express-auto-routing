import express, { Request, Response, NextFunction } from 'express'
import autoRouting, { Route } from '../src'

function testMiddleware(req: Request, res: Response, next: NextFunction) {
  console.log(req.method)
  next()
}

const port = 3000
const middlewareMapping = {
  testMiddleware
}
const app = express()
autoRouting(app, {
  middlewareMapping,
  callBack: (app) => {
    app.listen(port, () => {
      console.log(`Listening: http://localhost:${port}`)
    })
  }
})

type MiddlewareNames = keyof typeof middlewareMapping
export type MyRoute<QueryProps, BodyProps, OutputProps> = Route<MiddlewareNames, QueryProps, BodyProps, OutputProps>